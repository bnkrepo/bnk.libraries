﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNK.Common;
using System.Drawing;
using System.Runtime.InteropServices;

namespace BNK.Utilities
{
    /// <summary>   Utilities. </summary>
    public class GeneralTools
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets a large file icon. </summary>
        ///
        /// <param name="strFilePath">  Full pathname of the file. </param>
        ///
        /// <returns>   The large file icon. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static Icon GetLargeFileIcon(string strFilePath)
        {
            try
            {
                IntPtr hImgLarge;
                SHFILEINFO shinfo = new SHFILEINFO();

                if (string.IsNullOrEmpty(strFilePath) == true)
                    return null;

                //Use this to get the large Icon
                hImgLarge = PInvoke.SHGetFileInfo(strFilePath, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), PInvoke.SHGFI_ICON | PInvoke.SHGFI_LARGEICON);

                //The icon is returned in the hIcon member of the shinfo
                //struct
                Icon icon = (Icon)System.Drawing.Icon.FromHandle(shinfo.hIcon).Clone();
                PInvoke.DestroyIcon(shinfo.hIcon);
                return icon;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}
