﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BNK.Extensions
{
    public static class UIControl
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   An ISynchronizeInvoke extension method that synchronized invoke. </summary>
        ///
        /// <param name="sync">     The sync to act on. </param>
        /// <param name="action">   The action. </param>
        ///-------------------------------------------------------------------------------------------------        
        public static void SynchronizedInvoke(this ISynchronizeInvoke sync, Action action)
        {
            // If the invoke is not required, then invoke here and get out.
            if (!sync.InvokeRequired)
            {
                // Execute action.
                action();

                // Get out.
                return;
            }

            // Marshal to the required context.
            sync.Invoke(action, new object[] { });
        }
    }
}
