﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNK.Utilities
{
    public static class DateTimeExt
    {        
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   A DateTime extension method that converts a value to a readable time. </summary>
        ///
        /// <param name="value">    The value to act on. </param>
        ///
        /// <returns>   value as a string. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static string ToReadableTime(this DateTime value)
        {
            //Source: http://www.danylkoweb.com/Blog/10-extremely-useful-net-extension-methods-8J
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - value.Ticks);
            double delta = ts.TotalSeconds;
            if (delta < 60)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }

            if (delta < 120)
            {
                return "a minute ago";
            }

            if (delta < 2700) // 45 * 60
            {
                return ts.Minutes + " minutes ago";
            }

            if (delta < 5400) // 90 * 60
            {
                return "an hour ago";
            }

            if (delta < 86400) // 24 * 60 * 60
            {
                return ts.Hours + " hours ago";
            }

            if (delta < 172800) // 48 * 60 * 60
            {
                return "yesterday";
            }

            if (delta < 2592000) // 30 * 24 * 60 * 60
            {
                return ts.Days + " days ago";
            }

            if (delta < 31104000) // 12 * 30 * 24 * 60 * 60
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }

            var years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            return years <= 1 ? "one year ago" : years + " years ago";
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   A DateTime extension method that query if 'date' is working day. </summary>
        ///
        /// <param name="date"> The date to act on. </param>
        ///
        /// <returns>   true if working day, false if not. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static bool IsWorkingDay(this DateTime date)
        {
            //Source: http://www.danylkoweb.com/Blog/10-extremely-useful-net-extension-methods-8J
            return date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   A DateTime extension method that query if 'date' is weekend. </summary>
        ///
        /// <param name="date"> The date to act on. </param>
        ///
        /// <returns>   true if weekend, false if not. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static bool IsWeekend(this DateTime date)
        {
            //Source: http://www.danylkoweb.com/Blog/10-extremely-useful-net-extension-methods-8J
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

    }
}
