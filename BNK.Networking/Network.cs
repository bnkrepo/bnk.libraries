﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Management;
using BNK.Common;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace BNK.Networking
{
    /// <summary>   Network Helper Class. </summary>
    public static class NetworkHelper
    {
        #region IP related methods

        ///-------------------------------------------------------------------------------------------------
        /// <summary> Gets all IPs. </summary>
        ///
        /// <returns> List of IPs. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static List<string> GetAllIPs()
        {
            try
            {
                List<string> ips = new List<string>();

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_NetworkAdapterConfiguration");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    try
                    {
                        object ip = queryObj["IPAddress"];

                        if (ip != null)
                        {
                            if ((ip is string[]) == false)
                                ips.Add(queryObj["IPAddress"].ToString());
                            else
                            {
                                ips.Add((ip as string[])[0]);
                            }
                        }
                    }
                    catch (Exception) { return null; }
                }

                return ips;
            }
            catch (ManagementException)
            {
                return null;
            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the first IP. </summary>
        ///
        /// <returns>   The first IP. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static string GetFirstIP()
        {
            List<string> ips = GetAllIPs();
            string ip = null;

            if (ips != null && ips.Count > 0)
            {
                return ips[0];
            }

            return ip;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets all IPs that start with the parameter string. </summary>
        ///
        /// <param name="strStart"> The start part. e.g. It is in the format "192.16" for the IP range 192.16x.xxx.xxx. </param>
        ///
        /// <returns>  List of all the IPs that start with the parameter string. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static List<string> GetAllIPsStartWith(string strStart)
        {
            List<string> ips = GetAllIPs();
            List<string> selectedIPs = null;

            if (ips != null && string.IsNullOrEmpty(strStart) == false)
            {
                return ips.Where(ip => ip.StartsWith(strStart) == true).ToList();
            }

            return selectedIPs;
        }

        #endregion

        #region MAC related methods

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets all MAC addresses. </summary>
        ///
        /// <returns> Listof all the MAC addresses. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static List<string> GetAllMACs()
        {
            try
            {
                List<string> ips = new List<string>();

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_NetworkAdapter");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    try
                    {
                        object ip = queryObj["MACAddress"];

                        if (ip != null)
                        {
                            if ((ip is string[]) == false)
                                ips.Add(queryObj["MACAddress"].ToString());
                            else
                            {
                                ips.Add((ip as string[])[0]);
                            }
                        }
                    }
                    catch (Exception) { return null; }
                }

                return ips;
            }
            catch (ManagementException)
            {
                return null;
            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the first MAC address. </summary>
        ///
        /// <returns>   The first MAC address. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static string GetFirstMAC()
        {
            List<string> macs = GetAllMACs();
            string mac = null;

            if (macs != null && macs.Count > 0)
            {
                return macs[0];
            }

            return mac;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets all MAC addresses that start with the given parameter. </summary>
        ///
        /// <param name="strStart"> The start part. e.g. it is in the format "00:50:5" for the MAC 00:50:5x:xx:xx:xx </param>
        ///
        /// <returns>   List of all the MAC addresses that start with the given parameter. </returns>
        ///-------------------------------------------------------------------------------------------------
        public static List<string> GetAllMACsStartWith(string strStart)
        {
            List<string> macs = GetAllMACs();
            List<string> selectedMACs = null;

            if (macs != null && string.IsNullOrEmpty(strStart) == false)
            {
                return macs.Where(mac => mac.StartsWith(strStart) == true).ToList();
            }

            return selectedMACs;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets a mac from ip. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="strIP">  The ip. </param>
        ///
        /// <returns>   The mac from ip. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static string GetMACFromIP(string strIP)
        {
            try
            {
                IPAddress strDstIP = IPAddress.Parse(strIP); // the destination IP address

                byte[] macAddr = new byte[6];
                uint macAddrLen = (uint)macAddr.Length;

                if (PInvoke.SendARP(BitConverter.ToInt32(strDstIP.GetAddressBytes(), 0), 0, macAddr, ref macAddrLen) != 0)
                    throw new InvalidOperationException("SendARP failed.");

                string[] str = new string[(int)macAddrLen];

                for (int i = 0; i < macAddrLen; i++)
                {
                    str[i] = macAddr[i].ToString("x2");
                }

                return string.Join(":", str);
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion

        #region Ports being used by processes

        //http://www.cheynewallace.com/get-active-ports-and-associated-process-names-in-c/
        // 

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the net stat ports. </summary>
        ///
        /// <returns>   The net stat ports. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static List<Port> GetNetStatPorts()
        {
            var Ports = new List<Port>();

            try
            {
                using (Process p = new Process())
                {

                    ProcessStartInfo ps = new ProcessStartInfo();
                    ps.Arguments = "-a -n -o";
                    ps.FileName = "netstat.exe";
                    ps.UseShellExecute = false;
                    ps.WindowStyle = ProcessWindowStyle.Hidden;
                    ps.RedirectStandardInput = true;
                    ps.RedirectStandardOutput = true;
                    ps.RedirectStandardError = true;

                    p.StartInfo = ps;
                    p.Start();

                    StreamReader stdOutput = p.StandardOutput;
                    StreamReader stdError = p.StandardError;

                    string content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                    string exitStatus = p.ExitCode.ToString();

                    if (exitStatus != "0")
                    {
                        // Command Errored. Handle Here If Need Be
                    }

                    //Get The Rows
                    string[] rows = Regex.Split(content, "\r\n");

                    foreach (string row in rows)
                    {
                        //Split it baby
                        string[] tokens = Regex.Split(row, "\\s+");

                        if (tokens.Length > 4 && (tokens[1].Equals("UDP") || tokens[1].Equals("TCP")))
                        {
                            string localAddress = Regex.Replace(tokens[2], @"\[(.*?)\]", "1.1.1.1");

                            Ports.Add(new Port
                            {
                                Protocol = localAddress.Contains("1.1.1.1") ? String.Format("{0}v6", tokens[1]) : String.Format("{0}v4", tokens[1]),
                                PortNumber = localAddress.Split(':')[1],
                                ProcessName = tokens[1] == "UDP" ? LookupProcess(Convert.ToInt16(tokens[4])) : LookupProcess(Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])),
                                Pid = Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Ports != null && Ports.Count > 0)
                {
                    Ports = Ports.OrderBy(port => port.Pid).ToList();
                }
            }

            return Ports;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Looks up a given key to find its associated process. </summary>
        ///
        /// <param name="pid">  The pid. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        private static string LookupProcess(int pid)
        {
            string procName;

            try
            {
                procName = Process.GetProcessById(pid).ProcessName;
            }
            catch (Exception)
            {
                procName = "-";
            }

            return procName;
        }

        #endregion

        #region Network Interface Enumeration

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the net interface list. </summary>
        ///
        /// <returns>   The net interface list. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static List<NetworkInterface> GetNetInterfaceList()
        {
            List<NetworkInterface> netInterfaceList = new List<NetworkInterface>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_NetworkAdapterConfiguration");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    NetworkInterface netInterface = new NetworkInterface();

                    try { netInterface.Description = queryObj["Description"].ToString(); }
                    catch (Exception) { }
                    try { netInterface.DHCPEnabled = queryObj["DHCPEnabled"].ToString(); }
                    catch (Exception) { }
                    try { netInterface.DNSHostName = queryObj["DNSHostName"].ToString(); }
                    catch (Exception) { }

                    try
                    {
                        object ip = queryObj["IPAddress"];

                        if (ip != null)
                        {
                            if (!(ip is string[]))
                                netInterface.IPAddress = queryObj["IPAddress"].ToString();
                            else
                            {
                                netInterface.IPAddress = (ip as string[])[0];
                            }
                        }

                        //     continue;
                    }
                    catch (Exception) { }

                    try
                    {
                        object subnet = queryObj["IPSubnet"];

                        if (subnet != null)
                        {
                            if (!(subnet is string[]))
                                netInterface.IPSubnet = queryObj["IPSubnet"].ToString();
                            else
                            {
                                netInterface.IPSubnet = (subnet as string[])[0];
                            }
                        }
                    }
                    catch (Exception) { }

                    try { netInterface.MACAddress = queryObj["MACAddress"].ToString(); }
                    catch (Exception) { }

                    if (string.IsNullOrEmpty(netInterface.IPAddress) == false)
                        netInterfaceList.Add(netInterface);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return netInterfaceList;
        }

        #endregion
    }

    /// <summary>   Network interface. </summary>
    public class NetworkInterface
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the description. </summary>
        ///
        /// <value> The description. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Description { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the dhcp enabled. </summary>
        ///
        /// <value> The dhcp enabled. </value>
        ///-------------------------------------------------------------------------------------------------

        public string DHCPEnabled { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name of the dns host. </summary>
        ///
        /// <value> The name of the dns host. </value>
        ///-------------------------------------------------------------------------------------------------

        public string DNSHostName { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the ip address. </summary>
        ///
        /// <value> The ip address. </value>
        ///-------------------------------------------------------------------------------------------------

        public string IPAddress { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the ip subnet. </summary>
        ///
        /// <value> The ip subnet. </value>
        ///-------------------------------------------------------------------------------------------------

        public string IPSubnet { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the mac address. </summary>
        ///
        /// <value> The mac address. </value>
        ///-------------------------------------------------------------------------------------------------

        public string MACAddress { get; set; }

        /// <summary>   Default constructor. </summary>
        public NetworkInterface()
        {

        }

    }

    /// <summary>   Port. </summary>
    public class Port
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Name
        {
            get
            {
                return string.Format("{0} ({1} port {2})", this.ProcessName, this.Protocol, this.PortNumber);
            }
            set { }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the pid. </summary>
        ///
        /// <value> The pid. </value>
        ///-------------------------------------------------------------------------------------------------

        public int Pid { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the port number. </summary>
        ///
        /// <value> The port number. </value>
        ///-------------------------------------------------------------------------------------------------

        public string PortNumber { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name of the process. </summary>
        ///
        /// <value> The name of the process. </value>
        ///-------------------------------------------------------------------------------------------------

        public string ProcessName { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the protocol. </summary>
        ///
        /// <value> The protocol. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Protocol { get; set; }
    }
}
