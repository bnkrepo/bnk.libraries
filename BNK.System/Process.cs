﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using BNK.Common;

namespace BNK.System
{
    /// <summary>   System process class. </summary>
    public class SystemProcess
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the process information. </summary>
        ///
        /// <returns>   The process information. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static List<SystemProcessInfo> GetProcessInfo()
        {
            List<SystemProcessInfo> processInfo = new List<SystemProcessInfo>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Process");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemProcessInfo info = new SystemProcessInfo();

                    info.Name = queryObj["Name"].ToString();
                    try { info.CommandLine = queryObj["CommandLine"].ToString(); }
                    catch (Exception) { info.CommandLine = ""; }
                    info.ParentProcessId = Int32.Parse(queryObj["ParentProcessId"].ToString());
                    info.ProcessId = Int32.Parse(queryObj["ProcessId"].ToString());
                    info.VirtualSize = Utility.FormatBytes((ulong)Int64.Parse(queryObj["VirtualSize"].ToString()));
                    info.WorkingSetSize = Utility.FormatBytes((ulong)Int64.Parse(queryObj["WorkingSetSize"].ToString()));

                    processInfo.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return processInfo;
        }
    }

    /// <summary>   Information about the system process. </summary>
    public class SystemProcessInfo
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Name { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the command line. </summary>
        ///
        /// <value> The command line. </value>
        ///-------------------------------------------------------------------------------------------------

        public string CommandLine { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the identifier of the parent process. </summary>
        ///
        /// <value> The identifier of the parent process. </value>
        ///-------------------------------------------------------------------------------------------------

        public int ParentProcessId { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the identifier of the process. </summary>
        ///
        /// <value> The identifier of the process. </value>
        ///-------------------------------------------------------------------------------------------------

        public int ProcessId { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size of the virtual. </summary>
        ///
        /// <value> The size of the virtual. </value>
        ///-------------------------------------------------------------------------------------------------

        public string VirtualSize { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size of the working set. </summary>
        ///
        /// <value> The size of the working set. </value>
        ///-------------------------------------------------------------------------------------------------

        public string WorkingSetSize { get; set; }

        /// <summary>   Default constructor. </summary>
        public SystemProcessInfo()
        {

        }
    }
}
