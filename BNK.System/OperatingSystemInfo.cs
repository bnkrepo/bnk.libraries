﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using BNK.Common;

namespace BNK.System
{
    /// <summary>   Operating system. </summary>
    public class OperatingSystem
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the operating system information. </summary>
        ///
        /// <returns>   The operating system information. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static OperatingSystemInfo GetOperatingSystemInfo()
        {
            OperatingSystemInfo osInfo = new OperatingSystemInfo();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {

                    osInfo.BuildNumber = queryObj["BuildNumber"].ToString();
                    osInfo.Caption = queryObj["Caption"].ToString();
                    osInfo.CountryCode = Int32.Parse(queryObj["CountryCode"].ToString());
                    osInfo.CurrentTimeZone = Int32.Parse(queryObj["CurrentTimeZone"].ToString());
                    osInfo.Description = queryObj["Description"].ToString();
                    osInfo.FreePhysicalMemory = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreePhysicalMemory"].ToString()));
                    osInfo.FreeSpaceInPagingFiles = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreeSpaceInPagingFiles"].ToString()));
                    osInfo.FreeVirtualMemory = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreeVirtualMemory"].ToString()));

                    var id = ManagementDateTimeConverter.ToDateTime(queryObj["InstallDate"].ToString());
                    osInfo.InstallDate = id.ToShortDateString() + " " + id.ToShortTimeString(); ;

                    var lastBootUp = ManagementDateTimeConverter.ToDateTime(queryObj["LastBootUpTime"].ToString());
                    osInfo.LastBootUpTime = lastBootUp.ToShortDateString() + " " + lastBootUp.ToShortTimeString();

                    var dt = ManagementDateTimeConverter.ToDateTime(queryObj["LocalDateTime"].ToString());
                    osInfo.LocalDateTime = dt.ToShortDateString() + " " + dt.ToShortTimeString();
                    osInfo.MaxProcessMemorySize = Utility.FormatBytes((ulong)Int64.Parse(queryObj["MaxProcessMemorySize"].ToString()));
                    osInfo.NumberOfProcesses = Int32.Parse(queryObj["NumberOfProcesses"].ToString());
                    osInfo.NumberOfUsers = Int32.Parse(queryObj["NumberOfUsers"].ToString());
                    osInfo.Organization = queryObj["Organization"].ToString();
                    osInfo.OSArchitecture = queryObj["OSArchitecture"].ToString();
                    osInfo.RegisteredUser = queryObj["RegisteredUser"].ToString();
                    osInfo.ServicePackMajorVersion = Int32.Parse(queryObj["ServicePackMajorVersion"].ToString());
                    osInfo.ServicePackMinorVersion = Int32.Parse(queryObj["ServicePackMinorVersion"].ToString());
                    osInfo.SystemDrive = queryObj["SystemDrive"].ToString();
                    osInfo.TotalVirtualMemorySize = Utility.FormatBytes((ulong)Int64.Parse(queryObj["TotalVirtualMemorySize"].ToString()));
                    osInfo.TotalVisibleMemorySize = Utility.FormatBytes((ulong)Int64.Parse(queryObj["TotalVisibleMemorySize"].ToString()));
                    osInfo.Version = queryObj["Version"].ToString();
                    osInfo.WindowsDirectory = queryObj["WindowsDirectory"].ToString();
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return osInfo;
        }
    }

    /// <summary>   Information about the operating system. </summary>
    public class OperatingSystemInfo
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the build number. </summary>
        ///
        /// <value> The build number. </value>
        ///-------------------------------------------------------------------------------------------------

        public string BuildNumber { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the caption. </summary>
        ///
        /// <value> The caption. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Caption { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the country code. </summary>
        ///
        /// <value> The total number of ry code. </value>
        ///-------------------------------------------------------------------------------------------------

        public int CountryCode { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the current time zone. </summary>
        ///
        /// <value> The current time zone. </value>
        ///-------------------------------------------------------------------------------------------------

        public int CurrentTimeZone { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the description. </summary>
        ///
        /// <value> The description. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Description { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the free physical memory. </summary>
        ///
        /// <value> The free physical memory. </value>
        ///-------------------------------------------------------------------------------------------------

        public string FreePhysicalMemory { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the free space in paging files. </summary>
        ///
        /// <value> The free space in paging files. </value>
        ///-------------------------------------------------------------------------------------------------

        public string FreeSpaceInPagingFiles { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the free virtual memory. </summary>
        ///
        /// <value> The free virtual memory. </value>
        ///-------------------------------------------------------------------------------------------------

        public string FreeVirtualMemory { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the date of the install. </summary>
        ///
        /// <value> The date of the install. </value>
        ///-------------------------------------------------------------------------------------------------

        public string InstallDate { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the time of the last boot up. </summary>
        ///
        /// <value> The time of the last boot up. </value>
        ///-------------------------------------------------------------------------------------------------

        public string LastBootUpTime { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the time of the local date. </summary>
        ///
        /// <value> The time of the local date. </value>
        ///-------------------------------------------------------------------------------------------------

        public string LocalDateTime { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size of the maximum process memory. </summary>
        ///
        /// <value> The size of the maximum process memory. </value>
        ///-------------------------------------------------------------------------------------------------

        public string MaxProcessMemorySize { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the organization. </summary>
        ///
        /// <value> The organization. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Organization { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the operating system architecture. </summary>
        ///
        /// <value> The operating system architecture. </value>
        ///-------------------------------------------------------------------------------------------------

        public string OSArchitecture { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the registered user. </summary>
        ///
        /// <value> The registered user. </value>
        ///-------------------------------------------------------------------------------------------------

        public string RegisteredUser { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the service pack major version. </summary>
        ///
        /// <value> The service pack major version. </value>
        ///-------------------------------------------------------------------------------------------------

        public int ServicePackMajorVersion { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the service pack minor version. </summary>
        ///
        /// <value> The service pack minor version. </value>
        ///-------------------------------------------------------------------------------------------------

        public int ServicePackMinorVersion { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the system drive. </summary>
        ///
        /// <value> The system drive. </value>
        ///-------------------------------------------------------------------------------------------------

        public string SystemDrive { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size of the total virtual memory. </summary>
        ///
        /// <value> The total number of virtual memory size. </value>
        ///-------------------------------------------------------------------------------------------------

        public string TotalVirtualMemorySize { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size of the total visible memory. </summary>
        ///
        /// <value> The total number of visible memory size. </value>
        ///-------------------------------------------------------------------------------------------------

        public string TotalVisibleMemorySize { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the version. </summary>
        ///
        /// <value> The version. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Version { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the pathname of the windows directory. </summary>
        ///
        /// <value> The pathname of the windows directory. </value>
        ///-------------------------------------------------------------------------------------------------

        public string WindowsDirectory { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the number of processes. </summary>
        ///
        /// <value> The total number of processes. </value>
        ///-------------------------------------------------------------------------------------------------

        public int NumberOfProcesses { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the number of users. </summary>
        ///
        /// <value> The total number of users. </value>
        ///-------------------------------------------------------------------------------------------------

        public int NumberOfUsers { get; set; }

        /// <summary>   Default constructor. </summary>
        public OperatingSystemInfo()
        {
        }
    }
}
