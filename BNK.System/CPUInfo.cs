﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace BNK.System
{
    /// <summary>   Information about the cpu. </summary>
    public class CPU
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the cpu information. </summary>
        ///
        /// <returns>   The cpu information. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static CPUInfo GetCPUInfo()
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Processor");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    CPUInfo cpu = new CPUInfo
                    {
                        AddressWidth = queryObj["AddressWidth"].ToString(),
                        Architecture = queryObj["Architecture"].ToString(),
                        Availability = queryObj["Availability"].ToString(),
                        Caption = queryObj["Caption"].ToString(),
                        CpuStatus = queryObj["CpuStatus"].ToString(),
                        CurrentClockSpeed = queryObj["CurrentClockSpeed"].ToString(),
                        CurrentVoltage = queryObj["CurrentVoltage"].ToString(),
                        DataWidth = queryObj["DataWidth"].ToString(),
                        Description = queryObj["Description"].ToString(),
                        Manufacturer = queryObj["Manufacturer"].ToString(),
                        MaxClockSpeed = queryObj["MaxClockSpeed"].ToString(),
                        Name = queryObj["Name"].ToString(),
                        NumberOfCores = queryObj["NumberOfCores"].ToString(),
                        NumberOfLogicalProcessors = queryObj["NumberOfLogicalProcessors"].ToString()
                    };

                    return cpu;
                }
            }
            catch (ManagementException)
            {

            }

            return null;
        }
    }

    /// <summary>   Cpu. </summary>
    public class CPUInfo
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the width of the address. </summary>
        ///
        /// <value> The width of the address. </value>
        ///-------------------------------------------------------------------------------------------------

        public string AddressWidth { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the architecture. </summary>
        ///
        /// <value> The architecture. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Architecture { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the availability. </summary>
        ///
        /// <value> The availability. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Availability { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the caption. </summary>
        ///
        /// <value> The caption. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Caption { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the cpu status. </summary>
        ///
        /// <value> The cpu status. </value>
        ///-------------------------------------------------------------------------------------------------

        public string CpuStatus { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the current clock speed. </summary>
        ///
        /// <value> The current clock speed. </value>
        ///-------------------------------------------------------------------------------------------------

        public string CurrentClockSpeed { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the current voltage. </summary>
        ///
        /// <value> The current voltage. </value>
        ///-------------------------------------------------------------------------------------------------

        public string CurrentVoltage { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the width of the data. </summary>
        ///
        /// <value> The width of the data. </value>
        ///-------------------------------------------------------------------------------------------------

        public string DataWidth { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the description. </summary>
        ///
        /// <value> The description. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Description { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the manufacturer. </summary>
        ///
        /// <value> The manufacturer. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Manufacturer { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the maximum clock speed. </summary>
        ///
        /// <value> The maximum clock speed. </value>
        ///-------------------------------------------------------------------------------------------------

        public string MaxClockSpeed { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Name { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the number of cores. </summary>
        ///
        /// <value> The total number of cores. </value>
        ///-------------------------------------------------------------------------------------------------

        public string NumberOfCores { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the number of logical processors. </summary>
        ///
        /// <value> The total number of logical processors. </value>
        ///-------------------------------------------------------------------------------------------------

        public string NumberOfLogicalProcessors { get; set; }

        /// <summary>   Default constructor. </summary>
        public CPUInfo()
        {

        }
    }
}
