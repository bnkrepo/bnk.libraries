﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using BNK.Common;

namespace BNK.System
{
    /// <summary>   Information about the hard disk. </summary>
    public class HardDisk
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the hdd information. </summary>
        ///
        /// <returns>   The hdd information. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static List<HardDiskInfo> GetHDDInfo()
        {
            List<HardDiskInfo> infoList = new List<HardDiskInfo>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_LogicalDisk");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    HardDiskInfo info = new HardDiskInfo();

                    try { info.Access = queryObj["Access"].ToString(); }
                    catch (Exception) { }
                    try { info.Caption = queryObj["Caption"].ToString(); }
                    catch (Exception) { }
                    try { info.Compressed = queryObj["Compressed"].ToString(); }
                    catch (Exception) { }
                    try { info.Description = queryObj["Description"].ToString(); }
                    catch (Exception) { }
                    try { info.DeviceID = queryObj["DeviceID"].ToString(); }
                    catch (Exception) { }
                    try { info.DriveType = queryObj["DriveType"].ToString(); }
                    catch (Exception) { }
                    try { info.FileSystem = queryObj["FileSystem"].ToString(); }
                    catch (Exception) { }
                    try { info.FreeSpace = Utility.FormatBytes((ulong)queryObj["FreeSpace"]); }
                    catch (Exception) { }
                    try { info.Name = queryObj["Name"].ToString(); }
                    catch (Exception) { }
                    try { info.Size = Utility.FormatBytes((ulong)queryObj["Size"]); }
                    catch (Exception) { }
                    try { info.VolumeName = queryObj["VolumeName"].ToString(); }
                    catch (Exception) { }
                    try { info.VolumeSerialNumber = queryObj["VolumeSerialNumber"].ToString(); }
                    catch (Exception) { }
                    try { info.ProviderName = queryObj["ProviderName"].ToString(); }
                    catch (Exception) { }

                    infoList.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return infoList;
        }
    }

    /// <summary>   Information about the hard disk. </summary>
    public class HardDiskInfo
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the access. </summary>
        ///
        /// <value> The access. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Access { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the caption. </summary>
        ///
        /// <value> The caption. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Caption { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the compressed. </summary>
        ///
        /// <value> The compressed. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Compressed { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the description. </summary>
        ///
        /// <value> The description. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Description { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the identifier of the device. </summary>
        ///
        /// <value> The identifier of the device. </value>
        ///-------------------------------------------------------------------------------------------------

        public string DeviceID { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the type of the drive. </summary>
        ///
        /// <value> The type of the drive. </value>
        ///-------------------------------------------------------------------------------------------------

        public string DriveType { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the file system. </summary>
        ///
        /// <value> The file system. </value>
        ///-------------------------------------------------------------------------------------------------

        public string FileSystem { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the free space. </summary>
        ///
        /// <value> The free space. </value>
        ///-------------------------------------------------------------------------------------------------

        public string FreeSpace { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Name { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the size. </summary>
        ///
        /// <value> The size. </value>
        ///-------------------------------------------------------------------------------------------------

        public string Size { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name of the volume. </summary>
        ///
        /// <value> The name of the volume. </value>
        ///-------------------------------------------------------------------------------------------------

        public string VolumeName { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the volume serial number. </summary>
        ///
        /// <value> The volume serial number. </value>
        ///-------------------------------------------------------------------------------------------------

        public string VolumeSerialNumber { get; set; }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets or sets the name of the provider. </summary>
        ///
        /// <value> The name of the provider. </value>
        ///-------------------------------------------------------------------------------------------------

        public string ProviderName { get; set; }

        /// <summary>   Default constructor. </summary>
        public HardDiskInfo()
        {
            Access = "-";
            Caption = "-";
            Compressed = "-";
            Description = "-";
            DeviceID = "-";
            DriveType = "-";
            FileSystem = "-";
            FreeSpace = "-";
            Name = "-";
            Size = "-";
            VolumeName = "-";
            VolumeSerialNumber = "-";
            ProviderName = "-";
        }
    }
}
