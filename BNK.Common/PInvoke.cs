﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace BNK.Common
{
    /// <summary>   Shfileinfo structure. </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        /// <summary>   The icon. </summary>
        public IntPtr hIcon;
        /// <summary>   Zero-based index of the icon. </summary>
        public IntPtr iIcon;
        /// <summary>   The attributes. </summary>
        public uint dwAttributes;
        /// <summary>   Name of the display. </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;
        /// <summary>   Name of the type. </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    };

    /// <summary>   Process Invoke class. </summary>
    public class PInvoke
    {
        /// <summary>   The shgfi displayname. </summary>
        public const uint SHGFI_DISPLAYNAME = 0x00000200;
        /// <summary>   The shgfi typename. </summary>
        public const uint SHGFI_TYPENAME = 0x400;
        /// <summary>   The shgfi icon. </summary>
        public const uint SHGFI_ICON = 0x100;
        /// <summary>   The shgfi large icon. </summary>
        public const uint SHGFI_LARGEICON = 0x0; // 'Large icon
        /// <summary>   The shgfi small icon. </summary>
        public const uint SHGFI_SMALLICON = 0x1; // 'Small icon

        // For getting an assembly Icon

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Sh get file information. </summary>
        ///
        /// <param name="pszPath">          Full pathname of the file. </param>
        /// <param name="dwFileAttributes"> The file attributes. </param>
        /// <param name="psfi">             [in,out] The psfi. </param>
        /// <param name="cbSizeFileInfo">   Information describing the size file. </param>
        /// <param name="uFlags">           The flags. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Destroys the icon described by hIcon. </summary>
        ///
        /// <param name="hIcon">    The icon. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        [DllImport("User32.dll")]
        public static extern int DestroyIcon(IntPtr hIcon);

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Net get universal name. </summary>
        ///
        /// <param name="lpLocalPath">  Full pathname of the local file. </param>
        /// <param name="dwInfoLevel">  The information level. </param>
        /// <param name="lpBuffer">     The buffer. </param>
        /// <param name="lpBufferSize"> [in,out] Size of the buffer. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        [DllImport("mpr.dll")]
        public static extern int WNetGetUniversalName(string lpLocalPath, int dwInfoLevel, IntPtr lpBuffer, ref int lpBufferSize);

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Sends ARP request. </summary>
        ///
        /// <param name="destIp">           Destination ip. </param>
        /// <param name="srcIP">            Source ip. </param>
        /// <param name="macAddr">          The mac address. </param>
        /// <param name="physicalAddrLen">  [in,out] Length of the physical address. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int destIp, int srcIP, byte[] macAddr, ref uint physicalAddrLen);
    }
}
